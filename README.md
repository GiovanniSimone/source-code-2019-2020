# Documentation of the Switching and Routing project class 2019-2020
The switching and routing project code repository is made available for the students of switching and routing class held by prof. Guido Maier at Politecnico di Milano.

# Laptop setup

## Tools
Download the last version of [Virtualbox](https://www.virtualbox.org/) together with the [extension pack](https://download.virtualbox.org/virtualbox/6.0.14/Oracle_VM_VirtualBox_Extension_Pack-6.0.14.vbox-extpack), and [Bitvise client SSH](https://www.bitvise.com/).

## Virtual machines
Download the following virtual machines to develop your project 

SDN projects: [SAR_project_x64.ova](https://www.dropbox.com/s/s6zevkfhi7oohc1/SAR_project_x64.ova?dl=0) for x64 based operative systems and [SAR_project_x86.ova](https://www.dropbox.com/s/yiqam5olaphm0w9/SAR_project_x86.ova?dl=0 ) for x86 based operative systems. 

NFV projects: [ubuntu 16.04 LTS-server.ova](https://www.dropbox.com/s/f5tho1f01ms9f8b/ubuntu%2016.04%20LTS-server.ova?dl=0) and [ubuntu 16.04 LTS-client.ova](https://www.dropbox.com/s/b60olfpisw0q15h/ubuntu%2016.04%20LTS-client.ova?dl=0)

